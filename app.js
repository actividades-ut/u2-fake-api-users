import { randomUUID } from 'node:crypto'
import express, { json } from 'express'
import cors from 'cors'
import bcrypt from 'bcrypt';
import dotenv from 'dotenv'
import { authenticateToken, generateAccessToken } from './utils/jwt.js';

dotenv.config()

const users = [{
  id: "1",
  name: "YoAna",
  group: "IDYGS83",
  email: "test@gmail.com",
  password: "$2b$10$OWG7nYHnV5OcoMhCA6FPJezRiOEV/SAC37vcLhKsSKORCs6iXI5Qq"
}]

const PORT = process.env.PORT ?? 1234

const app = express()
app.use(cors())
app.use(json())

app.get('/users', (req, res) => {
  res.json(users)
})

app.get('/user', (req, res) => {
  const email = req.query.email
  
  if(typeof email !== "string") {
    return res.status(400).json({
      message: "Necesitas ingresar el email :p"
    })
  }

  const userFind = users.find((user) => user.email === email)

  if(!userFind) {
    return res.status(404).json({
      message: "Usuario no encontrado"
    })
  }
  res.json(userFind)
})

app.post('/register', async (req, res) => {
  const {email, password, group, name} = req.body

  if(typeof email !== "string" || typeof password !== "string" ||
    typeof group !== "string" || typeof name !== "string" ) {
    return res.status(400).json({
      message: "No estás ingresando los campos requeridos {email: string, password: string, group: string, name: string}"
    })
  }

  const repeatEmail = users.findIndex(user => user.email === email)

  if(repeatEmail !== -1) {
    return res.status(400).json({
      message: "El email ya está registrado"
    })
  }

  const hashPassword = await bcrypt.hash(password, 10)

  const newUser = {
    id: randomUUID(),
    email, 
    password: hashPassword, 
    group, 
    name
  }
  users.push(newUser)

  res.json({
    message: "Usuario creado exitosamente"
  })
})

app.post('/login', async (req, res) => {
  const {email, password} = req.body

  if(typeof email !== "string" || typeof password !== "string") {
    return res.status(400).json({
      message: "Necesitas ingresar el email y/o password :p"
    })
  }

  const userFind = users.find(user => user.email === email)

  if(!userFind) {
    return res.status(404).json({
      message: "Credenciales invalidas"
    })
  }

  const comparePassword = await bcrypt.compare(password, userFind.password)

  if(!comparePassword) {
    return res.status(404).json({
      message: "Credenciales invalidas"
    })
  }

  const token = generateAccessToken(userFind.id)

  res.json({
    message: "Inicio de sesión exitoso",
    token
  })
})

app.get('/profile', authenticateToken, (req, res) => {
  const payload = req.payload

  const findUser = users.find((user) => user.id === payload.id)

  if(!findUser) {
    return res.status(404).json({
      message: "Usuario no encontrado"
    })
  }

  res.json({
    result: "Felicidades toma una galleta!!",
    data: findUser
  })
})

app.put('/user/:id', authenticateToken , (req, res) => {
  const {email, group, name} = req.body
  const payload = req.payload
  const idUser = req.params.id

  if(typeof email !== "string" ||
    typeof group !== "string" || typeof name !== "string" ) {
    return res.status(400).json({
      message: "No estás ingresando los campos requeridos {email: string, group: string, name: string}"
    })
  }

  const userFindIndex = users.findIndex(user => user.id === idUser)

  if(userFindIndex === -1) {
    return res.status(404).json({
      message: "Usuario no encontrado"
    })
  }

  if(idUser !== payload.id) {
    return res.status(403).json({
      message: "No tienes autorizado actualizar este usuario"
    })
  }

  users[userFindIndex] = {
    ...users[userFindIndex],
    email, group, name
  }

  res.json({
    message: "Usuario actualizado de manera exitosa :D"
  })
})

app.post('/recovery', async (req, res) => {
  const {email, password, newpassword} = req.body
  
  if(typeof email !== "string" || typeof password !== "string" ||
    typeof newpassword !== "string") {
    return res.status(400).json({
      message: "No estás ingresando los campos requeridos {email: string, password: string, newpassword: string}"
    })
  }

  const userFind = users.find(user => user.email === email)

  if(!userFind) {
    return res.status(404).json({
      message: "Usuario no encontrado"
    })
  }

  const comparePassword = await bcrypt.compare(password, userFind.password)

  if(!comparePassword) {
    return res.status(400).json({
      message: "Contraseña antigua invalida"
    })
  }

  userFind.password = await bcrypt.hash(newpassword, 10)
  console.log(users)

  res.status(200).json({
    message: "Cambio de contraseña exitoso"
  })

})

app.use((req, res) => {
  res.sendStatus(404)
})

app.listen(PORT, () => {
  console.log(`Server listening on port http://localhost:${PORT}`)
})