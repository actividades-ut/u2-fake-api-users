import jwt from 'jsonwebtoken'

function generateAccessToken(id) {
  return jwt.sign({id}, process.env.TOKEN_SECRET)
  // return jwt.sign(username, process.env.TOKEN_SECRET, { expiresIn: '1800s' });
}

function authenticateToken(req, res, next) {
  const authHeader = req.headers['authorization']
  const token = authHeader && authHeader.split(' ')[1]

  if (!token) return res.sendStatus(401)

  jwt.verify(token, process.env.TOKEN_SECRET, (err, payload) => {

    if (err) return res.sendStatus(403) //Lo proceso pero aún así no estás autorizado

    req.payload = payload

    next()
  })
}
export {
  generateAccessToken,
  authenticateToken
}